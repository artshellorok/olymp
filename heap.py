# структура данных куча, не нашел аналога set на питоне поэтому написал свою реализацию
# from heap import Heap
# https://habr.com/ru/post/112222/


class Heap:
    memory = []

    def size(self):
        return len(self.memory) - 1

    def push(self, num):
        self.memory.append(num)
        parent = (self.size() - 1) // 2
        index = self.size()
        while index > 0:
            if self.memory[parent] > self.memory[index]:
                self.memory[parent], self.memory[index] = self.memory[
                    index], self.memory[parent]
                index = parent
                parent = (index - 1) // 2
            else:
                break

    def min(self):
        if self.memory:
            return self.memory[0]
        else:
            return None

    def heapify(self):
        index = 0
        while 2 * index + 2 <= self.size():
            root = self.memory[index]
            if self.memory[2 * index + 1] < self.memory[2 * index + 2]:
                minimum = 2 * index + 1
            else:
                minimum = 2 * index + 2
            if self.memory[minimum] < root:
                self.memory[minimum], self.memory[index] = self.memory[
                    index], self.memory[minimum]
                index = minimum
            else:
                break
        if 2 * index + 1 <= self.size():
            root = self.memory[index]
            if self.memory[2 * index + 1] < root:
                self.memory[
                    2 * index +
                    1], self.memory[index] = self.memory[index], self.memory[
                        2 * index + 1]

    def del_min(self):
        self.memory[0], self.memory[self.size()] = self.memory[
            self.size()], self.memory[0]
        self.memory.pop()
        self.heapify()
